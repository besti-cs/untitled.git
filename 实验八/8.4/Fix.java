import java.util.Stack;

public class Fix {

    public static String transform(String InString) {          //将中缀表达式转化成后缀表达式
        Stack<Character> stack = new Stack<Character>();
        String PoseString = "";
        for (int i = 0; i < InString.length(); i++) {
            Character temp;
            char c = InString.charAt(i);
            switch (c) {
                case ' ':
                    break;
                case '(':
                    stack.push(c);
                    break;
                case '+':
                case '-':
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == '(') {
                            stack.push('(');
                            break;
                        }
                        PoseString += " " + temp;
                    }
                    stack.push(c);
                    PoseString += " ";
                    break;
                case '*':
                case '/':
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == '(' || temp == '+' || temp == '-') {
                            stack.push(temp);
                            break;
                        } else {
                            PoseString += " " + temp;
                        }
                    }
                    stack.push(c);
                    PoseString += " ";
                    break;
                case ')':
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == '(')
                            break;
                        else
                            PoseString += " " + temp;
                    }
                    break;
                default:
                    PoseString += c;
            }
        }

        while (stack.size() != 0) {
            PoseString += " " + stack.pop();
        }
        return PoseString;
    }
}