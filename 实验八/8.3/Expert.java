import java.util.Scanner;

public class Expert {
    private LinkedBinaryTree<String> tree2;

    public Expert(){
        String a1 = "开始游戏(y/n)  ";
        String a2 = "不玩？！(y/n)";
        String a3 = "不玩就算了";
        String a4 = "不给你玩了";
        String a5 = "前面有一个草丛，是否绕路 (y/n)";
        String a6 = "有一个盖伦拿着大宝剑蹲在草丛，你挂掉了";
        String a7 = "明智的选择，你继续前进，来到一个分叉路口，一个老伯说让你向左走，是否相信他(y/n)";
        String a8 = "不听老人言，吃亏在眼前，你迷路了";
        String a9 = "顺利到家,是否睡觉(y/n)";
        String a10 ="滚去睡觉";
        String a11 ="通关";
        LinkedBinaryTree<String> n1,n2,n5,n6,n7,n8,n9,n10,n11,n12;
        n1 =new LinkedBinaryTree(a3);
        n2 =new LinkedBinaryTree(a4);
        n5 =new LinkedBinaryTree(a6);
        n6 =new LinkedBinaryTree(a8);
        n11=new LinkedBinaryTree(a10);
        n12=new LinkedBinaryTree(a11);
        n9 =new LinkedBinaryTree(a9,n12,n11);//a11,a10
        n10=new LinkedBinaryTree(a7,n9,n6);
        n7 =new LinkedBinaryTree(a2,n1,n2);//a3,a4
        n8 =new LinkedBinaryTree(a5,n10,n5);//a6,a7

        tree2 = new LinkedBinaryTree(a1,n8,n7);
    }

    public void start() throws EmptyCollectionException {
        Scanner scan = new Scanner(System.in);

        System.out.println("20202316饶坤的3A大作:回家睡觉");
        while(tree2.size() > 1)
        {
            System.out.println(tree2.getRootElement());
            if(scan.nextLine().equalsIgnoreCase("Y"))
                tree2 = tree2.getLeft();
            else
                tree2 = tree2.getRight();
        }
        System.out.println(tree2.getRootElement());
    }
}