import junit.framework.TestCase;

public class LinkedBinaryTree1Test extends TestCase {
    LinkedBinaryTree1 a1 = new LinkedBinaryTree1(88);
    LinkedBinaryTree1 a2 = new LinkedBinaryTree1(6);
    LinkedBinaryTree1 a3 = new LinkedBinaryTree1(20, a1, a2);
    LinkedBinaryTree1 a4 = new LinkedBinaryTree1(20);
    LinkedBinaryTree1 a5 = new LinkedBinaryTree1(2316, a3, a4);
    LinkedBinaryTree1 a6 = new LinkedBinaryTree1();

    public void testGetRight() throws EmptyCollectionException {
        System.out.println(a5.getRight().toString());
    }

    public void testContains() {
        assertEquals(true, a5.contains(2316));
        assertEquals(false, a1.contains(100));
    }

    public void testToString() {
        System.out.println(a5.toString());
    }

    public void testLevelOrder() throws EmptyCollectionException {
        assertEquals("[2316, 20, 20, 88, 6]",a5.levelorder().toString());
    }
    public void testPreorder() {
        assertEquals("[2316, 20, 88, 6, 20]",a5.preorder().toString());
    }

    public void testGetRootElement() throws EmptyCollectionException {
        assertEquals(2316, a5.getRootElement());
    }

    public void testPostorder() {
        assertEquals("[88, 6, 20, 20, 2316]",a5.postorder().toString());
    }

    public void testSize() {
        assertEquals(3, a3.size());
        assertEquals(5, a5.size());
    }

    public void testIsEmpty() {
        assertEquals(false,a1.isEmpty());
        assertEquals(true,a6.isEmpty());
    }

    public void testFind() throws ElementNotFoundException {
        assertEquals(2316 ,a5.find(2316));
    }
}