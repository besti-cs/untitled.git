import junit.framework.TestCase;

public class LinkedBinaryTreeTest extends TestCase {

    LinkedBinaryTree m = new LinkedBinaryTree();
    BinaryTreeNode tree;

    public void testTree(){
        char[] cd = {'A','B','D','H','I','E','J','M','N','C','F','G','K','L'};
        char[] on = {'H','D','I','B','E','M','J','N','A','F','C','K','G','L'};
        tree = m.construct(cd,on);
        System.out.println("中序：'A','B','D','H','I','E','J','M','N','C','F','G','K','L'");
        System.out.println("后序：'H','D','I','B','E','M','J','N','A','F','C','K','G','L'");
        assertEquals("H I D M N J E B F K L G C A ", m.poseOrder(tree));
    }
}