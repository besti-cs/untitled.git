import java.util.ArrayList;
import java.util.Arrays;

public class LinkedBinaryTree<T> implements BinaryTree<T> {
    protected BinaryTreeNode<T> root;

    public LinkedBinaryTree() {
        root = null;
    }

    public LinkedBinaryTree(T element) {
        root = new BinaryTreeNode<T>(element);
    }

    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right) {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException("Get root operation "
                    + "failed. The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree<T> getLeft() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }

    public T find(T target) throws ElementNotFoundException {
        BinaryTreeNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }

    //返回大小
    public int size() {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    public LinkedBinaryTree<T> getRight() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException("Get right operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();

        return result;
    }

    public boolean contains(T target) {
        if (root.find(target) == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isEmpty() {
        if (root == null) {
            return true;
        } else {
            return false;
        }
    }

    //先序
    public ArrayList<T> preorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.preorder(iter);

        return iter;
    }

    //中序
    public ArrayList<T> inorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.inorder(iter);

        return iter;
    }

    //后序
    public ArrayList<T> postorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.postorder(iter);

        return iter;
    }

    //层序
    public ArrayList<T> levelorder() throws EmptyCollectionException {
        LinkedQueue<BinaryTreeNode<T>> queue = new LinkedQueue<BinaryTreeNode<T>>();
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null) {
            queue.enqueue(root);
            while (!queue.isEmpty()) {
                BinaryTreeNode<T> current = queue.dequeue();

                iter.add(current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }
        return iter;
    }

    public String toString() {
        return super.toString();
    }

    public BinaryTreeNode construct(char[] pre, char[] in){
        if (pre.length == 0 || in.length == 0) {
            return null;
        }
        BinaryTreeNode<Character> tree = new BinaryTreeNode<Character>(pre[0]);
        int index = search(0, in.length, in, tree.getElement());
        tree.setLeft(construct(Arrays.copyOfRange(pre, 1, index + 1), Arrays.copyOfRange(in, 0, index)));
        tree.setRight(construct(Arrays.copyOfRange(pre, index + 1, pre.length),
                Arrays.copyOfRange(in, index + 1, in.length)));
        return tree;
    }

    public int search(int start, int end, char[] inOrders, char data) {
        for (int i = start; i < end; i++) {
            if (data == inOrders[i]) {
                return i;
            }
        }
        return -1;
    }

    String poseOrder = "";
    public String poseOrder(BinaryTreeNode tree) {
        BinaryTreeNode<T> leftTree = tree.left;
        if(leftTree != null){
            poseOrder(leftTree);
        }
        BinaryTreeNode<T> rightTree = tree.right;
        if(rightTree != null){
            poseOrder(rightTree);
        }
        poseOrder = poseOrder + tree.print() + " ";
        return poseOrder;
    }
}