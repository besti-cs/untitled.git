package Test;

import cn.edu.besti.cs2023.R2316.Searching;
import cn.edu.besti.cs2023.R2316.Sorting;
import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

    public  class test extends TestCase {

        Searching a = new Searching();
        Sorting m = new Sorting();

        @Test
        public void SortingTest() {
            Comparable[] s1 = {20, 49, 23, 123, 33, 55, 22, 202316};
            m.selectionSort(s1);
            assertEquals("20 22 23 33 49 55 123 202316 ", m.print(s1));             //正常乱序排序

            Comparable[] s2 = {1, 3, 6, 4, 2, 9, 11, 23, 202316};
            m.selectionSort(s2);
            assertEquals("1 2 3 4 6 9 11 23 202316 ", m.print(s2));              //正常乱序排序

            Comparable[] s3 = {99, 999, 9999, 10000, 100000};
            m.selectionSort(s3);
            assertEquals("99 999 9999 10000 100000 ", m.print(s3));             //正序排序

            Comparable[] s4 = {5201314, 52013, 520, 52, 5};
            m.selectionSort(s4);
            assertEquals("5 52 520 52013 5201314 ", m.print(s4));             //逆序排序

            Comparable[] s5 = {99, 98, 92, 12, 202316};
            m.selectionSort(s5);
            assertEquals("12 92 98 99 202316 ", m.print(s5));             //逆序排序

            Comparable[] s6 = {20, 20, 23, 16};
            m.selectionSort(s6);
            assertEquals("16 20 20 23 ", m.print(s6));                     //学号测试用例
        }

        @Test
        public void SearchingTest() {
            int[] k1 = {2, 3, 41, 543, 23, 657, 34, 12, 56, 202316};
            assertEquals(false, a.linearSearch(k1, 9, 10));            //正常
            assertEquals(true, a.linearSearch(k1, 2, 10));             //正常
            assertEquals("error", a.linearSearch(k1, 1, 100));       //异常
            assertEquals(true, a.linearSearch(k1, 202316, 10));           //边界

            int[] k2 = {5, 2, 7, 43, 21, 65, 23, 99, 202316};
            assertEquals(true, a.linearSearch(k2, 99, 8));//正常
            assertEquals(false, a.linearSearch(k2, 8, 7));  //正常
            assertEquals("error", a.linearSearch(k2, 1, 100)); //异常
            assertEquals(true, a.linearSearch(k2, 202316, 7));    //边界

            int[] k3 = {1, 24, 11, 11, 33, 33, 45, 202316};
            assertEquals(false, a.linearSearch(k3, 0, 8));//正常
            assertEquals("error", a.linearSearch(k3, 1, 18));//异常
            assertEquals(true, a.linearSearch(k3, 202316, 8)); //边界

            int[] k4 = {2, 422, 997, 435, 32, 45, 4, 43, 202316};
            assertEquals(true, a.linearSearch(k4, 4, 9));
            assertEquals(false, a.linearSearch(k4, 123, 9));
            assertEquals("error", a.linearSearch(k4, 3, 77));
            assertEquals(true, a.linearSearch(k4, 202316, 9));

            int[] k5 = {996, 65, 520, 56, 1314, 46, 23};
            assertEquals(true, a.linearSearch(k5, 1314, 7));
            assertEquals(false, a.linearSearch(k5, 12, 7));
            assertEquals("error", a.linearSearch(k5, 9, 99));
            assertEquals(true, a.linearSearch(k5, 996, 7));
        }
    }