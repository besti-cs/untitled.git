package paixu;
import java.util.Arrays;
public class Sorting
{

    //选择排序
    public static <T extends Comparable<T>> String selectionSort(T[] data)
    {
        int a;
        T temp;

        for (int index = 0; index < data.length-1; index++)
        {
            a = index;
            for (int scan = index+1; scan < data.length; scan++)
                if (data[scan].compareTo(data[a])<0)
                   a = scan;

            swap(data, a, index);
        }
        return Arrays.toString(data);
    }
    private static <T extends Comparable<T>> void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    //插入排序法
    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;
            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
            }

            data[position] = key;
        }
    }

    //冒泡排序法
    public static <T extends Comparable<T>>  void bubbleSort(T[] data)
    {
        int position, scan;
        T temp;

        for (position =  data.length - 1; position >= 0; position--)
        {
            for (scan = 0; scan <= position - 1; scan++)
            {
                if (data[scan].compareTo(data[scan+1]) > 0)
                    swap(data, scan, scan + 1);
            }
        }
    }
        //希尔排序
    public static void ShellSort(Comparable[] data) {
        int j = 0, temp = 0, k = 2;
        for (int increment = data.length / k; increment > 0; increment /= k) {
            for (int i = increment; i < data.length; i++) {
                temp = (int) data[i];
                for (j = i - increment; j >= 0; j -= increment) {
                    if (temp < (int) data[j]) {
                        data[j + increment] = data[j];
                    } else {
                        break;
                    }
                }
                data[j + increment] = temp;
            }
        }
    }

    //堆排序
    public static void HeapSort(int[] arr) {
        for (int i = arr.length / 2 - 1; i >= 0; i--) {
            adjustHeap(arr, i, arr.length);
        }
        for (int j = arr.length - 1; j > 0; j--) {
            swap(arr, 0, j);
            adjustHeap(arr, 0, j);
        }
    }

    public static void adjustHeap(int[] arr, int i, int length) {
        int temp = arr[i];
        for (int k = i * 2 + 1; k < length; k = k * 2 + 1) {
            if (k + 1 < length && arr[k] < arr[k + 1]) {
                k++;
            }
            if (arr[k] > temp) {
                arr[i] = arr[k];
                i = k;
            } else {
                break;
            }
        }
        arr[i] = temp;
    }

    public static void swap(int[] arr, int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }
}