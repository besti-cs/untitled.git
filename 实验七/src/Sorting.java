public class Sorting {
    public static void selectionSort (Comparable[] data)
    {
        int cp;
        for (int index = 0; index < data.length-1; index++)
        {
            cp = index;
            for (int scan = index+1; scan < data.length; scan++)
                if (data[scan].compareTo(data[cp]) < 0)
                    cp = scan;
            swap (data, cp, index);
        }
    }

    private static void swap (Comparable[] data, int index1, int index2)
    {
        Comparable temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    public String print(Comparable[] data){
        String result = "";
        for(int i=0;i<data.length;i++)
            result += ""+data[i]+" ";
        return result;
    }
}