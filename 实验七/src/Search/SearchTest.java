package Search;
import java.util.Scanner;

public class SearchTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Searching sea= new Searching();
        int target = 0;
        int i;
        int j=0;
        int[] arr={23,24,35,2020,2329};
        System.out.println("数据:\n"+sea.print(arr));

        System.out.println("顺序查找，输入你想要查找的数:");
        target = scan.nextInt();
        System.out.println("是否找到:"+sea.order(arr,target));
        target = scan.nextInt();
        System.out.println("是否找到:"+sea.order(arr,target));

        System.out.println("线性探查法查找，输入要查找的数:");
        target = scan.nextInt();
        int result[] = sea.hash(arr);
        int xianxing= sea.hashSearch(result,target);
        if(xianxing==0)
            System.out.println("failure");
        else
            System.out.println("success,查到的数为:"+xianxing);


       System.out.println("二叉排序树查找，输入要查找的数:");
        int a = scan.nextInt();
        Compareable target1;
        target1 = new Compareable(a);
        int[] b = new int[12];
        BinaryTree n=new BinaryTree();
        n.s(arr);
        Compareable tree = n.get();
        boolean k = treeSearch.cha(tree,target1);
        System.out.println("是否找到:"+k);

      /* System.out.println("链地址法查找，输入要查找的数:");
        target = scan.nextInt();
        Linked[] linked = new Linked[12];
        for(i=0;i<12;i++)
            linked[i] = new Linked(arr[i]);
        Linked[] re3 = sea.linkedHash(linked);
        int link = sea.linkedSearch(re3,target);
        if(link==0)
            System.out.println("failure");
        else
            System.out.println("success,查到的数为："+link);

        System.out.println("插值查找，输入要查找的数:");
        target = scan.nextInt();
        sea.sort(arr);
        System.out.println("查找到的数是:"+sea.InsertionSearch(arr,target,0,11));*/

       System.out.println("斐波那契查找，输入要查找的数:");
        target = scan.nextInt();
        System.out.println("是否找到:"+sea.FibonacciSearch(arr,target));

        System.out.println("分块查找，输入要查找的数:");
        target = scan.nextInt();
        int re1 = sea.blocking(arr,target);
        if(re1==0)
            System.out.println("查找失败！数组中无此数！");
        else
            System.out.println("查找成功！查找到的数是:"+re1);
        target = scan.nextInt();
        int re2 = sea.blocking(arr,target);
        if(re2==0)
            System.out.println("查找失败！数组中无此数！");
        else
            System.out.println("查找成功！查找到的数是:"+re2);

        int[] arr2 = {1,2,3,6,7,33,34,99,100,121,222,299,300};
        System.out.println("数据2:\n"+sea.print(arr2));
        System.out.println("折半查找，输入要查找的数:");
        target = scan.nextInt();
        System.out.println("是否找到:"+sea.binary(arr2,0,12,6,target));
        System.out.println("找到的数是:"+sea.binaryshow(arr2,0,12,6,target));
    }
}