package cn.edu.besti.cs2023.R2316;

public class Searching {
    public static Comparable linearSearch(int[] cp,int target,int length){
        int i=0;
        int m=target;
        if(length>cp.length){
            return"error";
        }
        else{
            while(cp[i]!=target){
                i++;
                if(i==cp.length){
                    break;
                }
            }
        }
        return i==cp.length?false:true;
    }
}