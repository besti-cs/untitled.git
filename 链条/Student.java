public class Student {
    String name;
    String id;
    String hobby;

    public Student(String name,String id,String hobby){
        this.name=name;
        this.id=id;
        this.hobby=hobby;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getId(){
        return id;
    }
    public void  setId(String id){
        this.id=id;
    }
    public String getHobby(){return hobby;}
    public void setHobby(String hobby){this.hobby=hobby;}

    @java.lang.Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", hobby='" + hobby + '\'' +
                '}';
    }
}

