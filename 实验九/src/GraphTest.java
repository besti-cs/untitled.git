import java.util.Stack;

public class GraphTest {
    public static void main(String[] args) {
        GraphOut graph = new GraphOut();
        System.out.println("该图的邻接表为：");
        outputGraph(graph);
    }
// 输出图的邻接表的方法。
    public static void outputGraph(GraphOut graph){
        for (int i=0;i<graph.verNum;i++){
            Vertex vertex = graph.verArray[i];
            System.out.print(vertex.verName);
            Edge current = vertex.edgeLink;
            while (current != null){
                System.out.print("-->"+current.tailName);
                current = current.broEdge;
            }
            System.out.println();
        }
    }
    public static void output(GraphOut graph, int[] a){

        Stack<Integer> stack = new Stack<>();
        for (int i=0;i<graph.verNum;i++){
            if(a[i]==0){
                System.out.println(i+1);
            }
        }
    }
}