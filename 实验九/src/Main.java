import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int v,e;
        int in,out;
        System.out.println("请输入顶点数和边数:");
        while(true){
            v = sc.nextInt();
            e = sc.nextInt();
            System.out.println("请按（头节点 尾节点 回车）的形式依次输入边的信息：");
            if(v==0&&e==0)
                break;
            Graph g = new Graph(v,e);
            for(int i=0;i<e;i++){
                in = sc.nextInt();
                out = sc.nextInt();
                g.insertEdge(in,out);
            }
            for(int i=0;i<v;i++){
                if(i!=0)
                    System.out.print(" ");
                System.out.print("\n第"+i+"个的出度为:"+g.outDegree(i));
            }
            System.out.print("\n=============================");
            for(int i=0;i<v;i++){
                if(i!=0)
                    System.out.print(" ");
                System.out.print("\n第"+i+"个的入度为:"+g.inDegree(i));
            }
            System.out.print("\n=============================");
        }
    }

}

class Graph{
    private int vertex;
    private int edge;
    private int graph[][];

    Graph(int v,int e){
        vertex = v;
        edge = e;
        graph = new int[v][v];
        for(int i=0;i<v;i++){
            for(int j=0;j<v;j++){
                graph[i][j]=0;
            }
        }
    }

    void insertEdge(int in,int out){
        graph[in-1][out-1] = 1;
    }

    int outDegree(int theVertex){
        int cnt=0;
        for(int i=0;i<vertex;i++){
            cnt += graph[theVertex][i];
        }
        return cnt;
    }

    int inDegree(int theVertex){
        int cnt=0;
        for(int i=0;i<vertex;i++){
            cnt += graph[i][theVertex];
        }
        return cnt;
    }
}