public class Prim {

    public static void  Prim(int [][] graph,int start,int n){
        int [][] min=new int [n][2];
        for(int i=0;i<n;i++){
            if(i==start){
                min[i][0]=-1;
                min[i][1]=0;
            }else if( graph[start][i]!=-1){//说明存在（start，i）的边
                min[i][0]=start;
                min[i][1]= graph[start][i];
            }else{
                min[i][0]=-1;
                min[i][1]=Integer.MAX_VALUE;
            }
        }
        for(int i=0;i<n-1;i++){
            int minV=-1,minW=Integer.MAX_VALUE;
            for(int j=0;j<n;j++){
                if(min[j][1]!=0&&minW>min[j][1]){
                    minW=min[j][1];
                    minV=j;
                }
            }
            min[minV][1]=0;
            System.out.println("最小生成树的第"+(i+1)+"条最小边是（"+(min[minV][0]+1)+","+(minV+1)+"），权重="+minW);
            for(int j=0;j<n;j++){
                if(min[j][1]!=0){
                    if( graph[minV][j]!=-1&& graph[minV][j]<min[j][1]){
                        min[j][0]=minV;
                        min[j][1]= graph[minV][j];
                    }
                }
            }
        }
    }
    public static void main(String [] args){
        int [][] tree={
                {-1,5,1,-1,6,-1},
                {5,-1,-1,8,-1,-1},
                {1,-1,-1,2,9,4},
                {-1,8,2,-1,3,4},
                {6,-1,9,4,-1,2},
                {-1,-1,4,3,2,-1}
        };
        Prim.Prim(tree, 0, 6);
    }
}