import java.util.*;
public class GraphLoopTest {
    private Map<String, List<String>> graph = new HashMap<String, List<String>>();
    public void InitGraphData() {
        graph.put("1", Arrays.asList("2", "3","4"));
        graph.put("2", Arrays.asList("1","5","6"));
        graph.put("3", Arrays.asList("1","7","8"));
        graph.put("4", Arrays.asList("1"));
        graph.put("5", Arrays.asList("2"));
        graph.put("6", Arrays.asList("2"));
        graph.put("7", Arrays.asList("3"));
        graph.put("8", Arrays.asList("3","4"));

    }

    /**
    队列来实现图
     */
    private Queue<String> queue = new LinkedList<String>();
    private Map<String, Boolean> status = new HashMap<String, Boolean>();
    public void BFSSearch(String startPoint) {
        //1.把起始点放入queue；
        queue.add(startPoint);
        status.put(startPoint, false);
        bfsLoop();
    }

    private void bfsLoop() {
        //  1) 从queue中取出队列头的点；更新状态为已经遍历。
        String currentQueueHeader = queue.poll(); //出队
        status.put(currentQueueHeader, true);
        System.out.println(currentQueueHeader);
        //  2) 找出与此点邻接的且尚未遍历的点，进行标记，然后全部放入queue中。
        List<String> neighborPoints = graph.get(currentQueueHeader);
        for (String poinit : neighborPoints) {
            if (!status.getOrDefault(poinit, false)) { //未被遍历
                if (queue.contains(poinit)) continue;
                queue.add(poinit);
                status.put(poinit, false);
            }
        }
        if (!queue.isEmpty()) {  //如果队列不为空继续遍历
            bfsLoop();
        }
    }

    private Stack<String> stack = new Stack<String>();
    public void DFSSearch(String startPoint) {
        stack.push(startPoint);
        status.put(startPoint, true);
        dfsLoop();
    }

    private void dfsLoop() {
        if(stack.empty()){
            return;
        }
        //查看栈顶元素，但并不出栈
        String stackTopPoint = stack.peek();
        //  2) 找出与此点邻接的且尚未遍历的点，进行标记，然后全部放入queue中。
        List<String> neighborPoints = graph.get(stackTopPoint);
        for (String point : neighborPoints) {
            if (!status.getOrDefault(point, false)) { //未被遍历
                stack.push(point);
                status.put(point, true);
                dfsLoop();
            }
        }
        String popPoint =  stack.pop();
        System.out.println(popPoint);
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请选择深度（0）还是广度遍历（1）");
        int choose = scan.nextInt();
        GraphLoopTest test = new GraphLoopTest();
        test.InitGraphData();
        if(choose==1){
            System.out.println("广度优先遍历 ：");
            test.BFSSearch("1");}
        if(choose==0){
            System.out.println("深度优先遍历： ");
            test.DFSSearch("1");}

    }
}