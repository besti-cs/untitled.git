import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

public class TuoPuTest {
    public static void main(String[] args) throws IOException {
        Scanner scan=new Scanner(System.in);
        System.out.println("进行的操作：拓扑排序(0);邻接表(1);顶点的出入度(2):");
        int cp = scan.nextInt();
        if(cp==0) {
            Tuopu directedGraph = new Tuopu();
            try {
                directedGraph.topoSort();
            } catch (Exception e) {
                System.out.println("出错了啦,有环");
                e.printStackTrace();
            }
        }
        if(cp==1) {
            GraphOut graph = new GraphOut();
            System.out.println("该图的邻接表为：");
            outputGraph(graph);
        }
        else {
            System.out.println("请输入顶点数和边数:");
            while(true){
                Scanner sc = new Scanner(System.in);
                int v,q;
                int in,out;
                v = sc.nextInt();
                q = sc.nextInt();
                System.out.println("请按（头节点 尾节点 回车）的形式依次输入边的信息：");
                if(v==0&&q==0)
                    break;
                Graph g = new Graph(v,q);
                for(int i=0;i<q;i++){
                    in = sc.nextInt();
                    out = sc.nextInt();
                    g.insertEdge(in,out);
                }
                for(int i=0;i<v;i++){
                    if(i!=0)
                        System.out.print(" ");
                    System.out.print("\n第"+i+"个的出度为:"+g.outDegree(i));
                }
                System.out.print("\n=============================");
                for(int i=0;i<v;i++){
                    if(i!=0)
                        System.out.print(" ");
                    System.out.print("\n第"+i+"个的入度为:"+g.inDegree(i));
                }
                System.out.print("\n=============================");
            }
        }

    }
    public static void outputGraph(GraphOut graph){
        for (int i=0;i<graph.verNum;i++){
            Vertex vertex = graph.verArray[i];
            System.out.print(vertex.verName);
            Edge current = vertex.edgeLink;
            while (current != null){
                System.out.print("-->"+current.tailName);
                current = current.broEdge;
            }
            System.out.println();
        }
    }
    public static void output(GraphOut graph, int[] a){

        Stack<Integer> stack = new Stack<>();
        for (int i=0;i<graph.verNum;i++){
            if(a[i]==0){
                System.out.println(i+1);
            }
        }
    }
    static class Graph{
        private int vertex;
        private int edge;
        private int graph[][];

        Graph(int v,int e){
            vertex = v;
            edge = e;
            graph = new int[v][v];
            for(int i=0;i<v;i++){
                for(int j=0;j<v;j++){
                    graph[i][j]=0;
                }
            }
        }

        void insertEdge(int in,int out){
            graph[in-1][out-1] = 1;
        }

        int outDegree(int theVertex){
            int cnt=0;
            for(int i=0;i<vertex;i++){
                cnt += graph[theVertex][i];
            }
            return cnt;
        }

        int inDegree(int theVertex){
            int cnt=0;
            for(int i=0;i<vertex;i++){
                cnt += graph[i][theVertex];
            }
            return cnt;
        }
    }
}