public class poly {
    public static void main(String[] args) {
        human op =new Stu();
        op.eathabits();
        Stu s=(Stu) op;
        s.study();
        human cp =new Police();
        cp.eathabits();
        Police m=(Police) cp;
        m.work();
    }
}
class human {
    public void eathabits() {
        System.out.println("饮食习惯");
    }
}

class Stu  extends human{
    public void eathabits(){
        System.out.println("爱吃辣也能吃辣");
    }
    public void study(){
        System.out.println("好好学习");
    }
}

class Police extends human{
    public void eathabits(){
        System.out.println("吃饭");
    }
    public void work(){
        System.out.println("维护治安，惩治罪恶");
    }
}
