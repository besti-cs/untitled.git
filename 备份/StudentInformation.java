public class StudentInformation {
    private String name;
    private String id;
    private int age;
    private String food;
    private int  score;
    public  int getAge(){
        return age;
    }
    public String getId(){
        return  id;
    }
    public String getName(){
        return name;
    }
    public int getScore() {
        return score;
    }
    public String getFood() {
        return food;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setFood(String food) {
        this.food = food;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setScore(int score) {
        this.score = score;
    }
}
