public class Complex {
    double a,b;
    Complex(double m,double n){//构造函数设置实部虚部
        a=m;
        b=n;
    }
    public double getRealPart(){//返回实部
        return a;
    }

    public double getImagePart() {//返回虚部
        return b;
    }
    public Complex ComplexAdd(Complex y){//加法
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a+m;
        double z=b+n;
        return new Complex(x,z);
    }
    public Complex ComplexSub(Complex y){
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a-m;
        double z=b-n;
        return new Complex(x,z);
    }
    public Complex ComplexMulti(Complex y){
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a*m;
        double z=b*n;
        return new Complex(x,z);
    }
    public Complex ComplexDiv(Complex y){
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a/m;
        double z=b/n;
        return new Complex(x,z);
    }


    public java.lang.String toString() {
        String s="";
        if (a!=0&&b>0&&b!=1){
            s+= a+"+"+ b+"i";
        }
        else if(a!=0&&b==1){
            s+=a+"+i";
        }
        else if (a!=0&&b<0&&b!=-1){
            s+= a+""+b+"i";
        }
        else if (a!=0&&b==-1){
            s+=a+"-i";
        }
        else if (a!=0&&b==0){
            s+=a;
        }
        else if (a==0&&b!=0){
            s+=b+"i";
        }
        else if (a==0&&b==0){
            s+="0.0";
        }
        return s;
    }
}