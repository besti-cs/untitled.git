public class StringBufferDemo {
    StringBuffer buffer;

    public static char CharAt(StringBuffer buffer, int index) {
        return buffer.charAt(index);
    }

    public static int Capacity(StringBuffer buffer) {
        return buffer.capacity();
    }

    public static int IndexOf(StringBuffer buffer, String str) {
        return buffer.indexOf(str);
    }

    public static String ToString(StringBuffer buffer) {
        return "buffer = " + buffer.toString();
    }

    public static int Length(StringBuffer buffer) {
        return buffer.length();
    }
}