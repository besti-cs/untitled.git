import java.util.Arrays;

/**
 * Created by besti on 2021/10/27.
 */
public class ArrayStack<T> implements StackADT<T>{
    T [] arrayStack2023;
    int top = 0;
    public ArrayStack(T[] arrayStack2023) {
        this.arrayStack2023 = arrayStack2023;
    }

    @java.lang.Override
    public void push(T element) {
        if (top != arrayStack2023.length){
            arrayStack2023[top++] = element;
        }else {
            System.out.println("数组满");//数组需要扩容了
            T [] newArray = (T[])(new Object[arrayStack2023.length*2]);
            this.arrayStack2023 = newArray;
        }
    }

    @java.lang.Override
    public T pop() {
        return null;
    }

    @java.lang.Override
    public T peek() {

        return arrayStack2023[top-1];
    }

    @java.lang.Override
    public boolean isEmpty() {
        return false;
    }

    @java.lang.Override
    public int size() {
        return 0;
    }

    @java.lang.Override
    public String toString() {
        return "ArrayStack{" +
                "arrayStack2023=" + Arrays.toString(arrayStack2023) +
                ", top=" + top +
                '}';
    }
}
