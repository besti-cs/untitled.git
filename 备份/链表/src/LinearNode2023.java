public class LinearNode2023<T> {
    public LinearNode2023<T> next;//节点的指针(引用)
    //    public LinearNode2023<T> prior;//节点的指针(引用)
    public T element;//节点属性、值
//    public LinearNode2023<T> rear;//节点的尾指针(引用)


    public LinearNode2023() {
        this.next = null;
        this.element = null;
    }

    public LinearNode2023(T element) {
        this.element = element;
        this.next = null;
    }

    @java.lang.Override
    public String toString() {
//        String temp = "姓名:"+this.getElement()"      学号      爱好\n";

        return "";
    }

    @java.lang.Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LinearNode2023)) return false;

        LinearNode2023<?> that = (LinearNode2023<?>) o;

        if (next != null ? !next.equals(that.next) : that.next != null) return false;
        return element != null ? element.equals(that.element) : that.element == null;
    }

    @java.lang.Override
    public int hashCode() {
        int result = next != null ? next.hashCode() : 0;
        result = 31 * result + (element != null ? element.hashCode() : 0);
        return result;
    }

    public LinearNode2023 getNext() {
        return next;
    }

    public void setNext(LinearNode2023 next) {
        this.next = next;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }
}
