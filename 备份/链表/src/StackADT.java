public interface StackADT<T>
{
   public void push(T element);//入
   public T peek();//取顶的元素
   public T pop();//出
    public int size();//有多少元素
    public  boolean isEmpty();//是否为空
}
