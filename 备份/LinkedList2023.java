public class LinkedList2023<T> {
//    LinearNode2023 head;

    public LinkedList2023() {
    }

    //头插法
    public LinearNode2023 insertHead(LinearNode2023 head, LinearNode2023 element){
        if(head ==null){
            head = element;
        }else {
            //1.头插法
//            element.next = head;//说明有头节点，如没有element.next = head
//            head = element;//说明有头节点，如没有head = element;
//            element.next = head.next;//说明有头节点，如没有element.next = head
//            head.next = element;//说明有头节点，如没有head = element;
//            2.尾插法
            LinearNode2023 pointer = head;
            while (pointer.next != null){
                pointer = pointer.next;
            }
            pointer.next = element;
        }
        return head;
    }
    public boolean searchElement(LinearNode2023 head,LinearNode2023 elementNode){
        LinearNode2023 pt =head;
        while (pt!=null && !pt.element.equals(elementNode.element)){
            pt = pt.next;
        }
        if (pt == null){
            return false;
        }else if (pt.element.equals(elementNode.element)){
            return true;
        }
        return false;
    }

    public String printLinkedList2023(LinearNode2023 head) {
        String temp = "";
        LinearNode2023 pointer = head;
        while (pointer != null){
            temp = temp + pointer.getElement().toString()+ "\n";
            pointer = pointer.next;
        }
        return temp;
    }

}
