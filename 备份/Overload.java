public class Overload {

        public int test(){
            System.out.println("HelloWorld");
            return 1;
        }

        public void test(int a){
            System.out.println("test1 bigin");
        }

        //以下两个式子内部的参数顺序不同。
        public String test(int a,String s){
            System.out.println("test2");
            return "你好";
        }

        public String test(String s,int a){
            System.out.println("test3");
            return "你好";
        }

        public static void main(String[] args){
            Overload o = new Overload();
            System.out.println(o.test());
            o.test(1);
            //前后顺序变换，最后输出顺序相同。
            System.out.println(o.test(1,"test2"));
            System.out.println(o.test("test3",1));
        }
    }