import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        //建立客户端，链接服务器地址：172.16.43.134.
        Socket socket = new Socket("172.16.43.134",8700);
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        OutputStream outputStream = socket.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        String sp = "用户名：20202316 密码：20021101q";
        outputStreamWriter.write(sp);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        String reply = null;
        while (!((reply = bufferedReader.readLine())==null)){
            System.out.println(reply);
        }
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        outputStream.close();
    }
}