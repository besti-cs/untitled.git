public class Animal {
    String name;
    String food;
    String speed;

    public Animal(String name ,String food,String speed){
        this.name=name;
        this.food=food;
        this.speed=speed;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getFood(){
        return food;
    }
    public void setFood(String food){
        this.food=food;
    }
    public String getSpeed(){
        return speed;
    }
    public void setSpeed(String speed){
        this.speed=speed;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", food='" + food + '\'' +
                ", speed='" + speed + '\'' +
                '}';
    }
}
