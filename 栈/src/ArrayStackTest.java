import junit.framework.TestCase;

public class ArrayStackTest extends TestCase {

    public void test(){

        ArrayStack<Animal> Stack = new ArrayStack<>();

        Stack.push("公鸡");
        Stack.push("吃玉米粒");
        Stack.push("跑的快");
        System.out.println(""+Stack);
        System.out.println("长度"+Stack.size());
        System.out.println("是否为空:"+Stack.isEmpty());
        assertEquals("跑的快",Stack.pop());
        assertEquals("吃玉米粒",Stack.peek());
        System.out.println("peek的元素："+Stack.peek());
        System.out.println("删除后的："+Stack);
    }
}

