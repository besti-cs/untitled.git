import java.util.Arrays;

public class ArrayStack<T> implements StackADT {
    final int love=1000;
    int top=0 ;
    public T[] stack;

    public ArrayStack(int eva){
        stack=(T[]) (new Object[eva]);
    }
    public ArrayStack() {
        stack = (T[]) (new Object[love]);
    }

    public void push(Object element){
        if (size() == stack.length)
            expandCapacity();
        stack[top] = (T) element;
        top++;
    }

    private void expandCapacity() {
        T[] larger = (T[]) (new Object[stack.length*2]);
        for(int index=0;index<stack.length;index++)
            larger[index] = stack[index];
        stack=larger;
    }

    public T pop() {
        if (isEmpty())
            System.out.println("stack");
        top--;
        T result = stack[top];
        stack[top] = null;

        return result;
    }

    public T peek(){
        if (isEmpty())
            System.out.println("stack");
        return stack[top-1];
    }

    public boolean isEmpty() {
        if(0==size()){
            return true;
        }else{
            return false;
        }
    }

    public int size() {
        return top;
    }

    public String toString() {
        return "ArrayStack:" +
                "arrayStack=" + Arrays.toString(stack) +
                ", top=" + top +
                '.';
    }
}